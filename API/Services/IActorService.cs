﻿using System.Collections.Generic;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;

namespace DemoApp.Services
{
    public interface IActorService
    {
        public IEnumerable<Person> GetAll();
        public Person GetEntity(int id);
        public void AddEntity(PersonRequest actor);
        public void UpdateEntity(PersonRequest actor, int id);
        public void DeleteEntity(int id);
        public IEnumerable<Person> GetEntitiesByMovieId(int movieId);

    }
}
