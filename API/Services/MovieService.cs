﻿using DemoApp.Models;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IProducerRepository _producerRepository;
        private readonly IGenreRepository _genreRepository;
      
      
   

        public MovieService(IMovieRepository movieRepository,
            IGenreRepository genreRepository,IActorRepository actorRepository,IProducerRepository producerRepository
            )

        {
           _actorRepository = actorRepository;
            _genreRepository = genreRepository;
            _producerRepository = producerRepository;
            _movieRepository = movieRepository;
         
        }


        public IEnumerable<MovieResponse> GetAll()
        {
            List<MovieResponse> listMovies = new List<MovieResponse>();

            var movies= _movieRepository.GetAll().ToList();
            foreach (var movie in movies)
            {
                var actors = _actorRepository.GetEntitiesByMovieId(movie.Id).ToList();
                var producer = _producerRepository.GetEntityByMovieId(movie.Id);
                var genres = _genreRepository.GetEntitiesByMovieId(movie.Id).ToList();
                var movieDetails = new MovieResponse() {
                                        Id = movie.Id,
                                        Name = movie.Name,
                                        Plot = movie.Plot,
                                        Profit = movie.Profit,
                                        Actors = actors,
                                        Producer = producer,
                                        Genres = genres,
                                        YearOfRelease = movie.YearOfRelease,
                                        Poster=movie.Poster
                                    };
                listMovies.Add(movieDetails);


            }
            return listMovies;


        }

        public MovieResponse GetEntity(int id)
        {
            var movie = _movieRepository.GetEntity(id);
            if (movie!= null)
            {
                var actors = _actorRepository.GetEntitiesByMovieId(movie.Id).ToList();
                var producer = _producerRepository.GetEntityByMovieId(movie.Id);
                var genres = _genreRepository.GetEntitiesByMovieId(movie.Id).ToList();
                var movieDetails = new MovieResponse()
                {
                    Id = movie.Id,
                    Name = movie.Name,
                    Plot = movie.Plot,
                    Profit = movie.Profit,
                    Actors = actors,
                    Producer = producer,
                    Genres = genres,
                    YearOfRelease = movie.YearOfRelease,
                    Poster = movie.Poster
                };
                return movieDetails;


            }
            throw new Exception();
           
        }
        public void AddEntity(MovieRequest movie)
        {
            _movieRepository.AddEntity(movie);
        }
        public void UpdateEntity(MovieRequest movie, int id)
        {
            var movieById = _movieRepository.GetEntity(id);
            if (movieById != null)
            {
                _movieRepository.UpdateEntity(movie, id);
            }
            else
            {
                throw new Exception();
            }
            
        }
        public void DeleteEntity(int id)
        {
            var movieById = _movieRepository.GetEntity(id);
            if (movieById != null)
            {
                _movieRepository.DeleteEntity(id);
            }
            else
            {
                throw new Exception();
            }
            
        }
       


    }
}
