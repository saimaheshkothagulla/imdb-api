﻿using DemoApp.Models;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> GetAll();
        public MovieResponse GetEntity(int id);
        public void AddEntity(MovieRequest movie);
        public void UpdateEntity(MovieRequest movie, int id);
        public void DeleteEntity(int id);
    }
}
