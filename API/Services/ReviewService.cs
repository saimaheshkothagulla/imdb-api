﻿using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;


        public ReviewService(IReviewRepository reviewRepository)    {
            _reviewRepository = reviewRepository;
        }

        public IEnumerable<Review> GetAll(int movieId)
        {
            var reviews = _reviewRepository.GetAll(movieId);
            if (reviews.Count()!= 0)
            {
              return _reviewRepository.GetAll(movieId);
                
            }
            throw new Exception();
            
        }

        public Review GetEntity(int movieId, int reviewId)
        {
            var review = _reviewRepository.GetEntity(movieId, reviewId);

            if (review != null)
            {
                    return review;

            }
            throw new Exception();
        }

        
        public void AddEntity(ReviewRequest review, int movieId)
        {
            try {
                _reviewRepository.AddEntity(review, movieId);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public void UpdateEntity(int movieId,ReviewRequest review, int reviewId)
        {
            var reviewById = _reviewRepository.GetEntity(movieId,reviewId);
            if (reviewById != null)
            {
                _reviewRepository.UpdateEntity(movieId,review, reviewId);
            }
            else
            {
                throw new Exception();
            }


        }
        public void DeleteEntity(int movieId,int reviewId)
        {
            try
            {
                _reviewRepository.DeleteEntity(movieId,reviewId);
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }
}
