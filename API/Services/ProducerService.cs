﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;
        public ProducerService(IProducerRepository producerRepository) {
            _producerRepository = producerRepository;
        
        }

        public IEnumerable<Person> GetAll()
        {
            return _producerRepository.GetAll();
        }

        public Person GetEntity(int id)
        {

            var producer=_producerRepository.GetEntity(id);
            if (producer != null)
            {
                return producer;
            }
            throw new Exception();
        }
        public Person GetEntityByMovieId(int movieId)
        {
            var producers= _producerRepository.GetEntityByMovieId(movieId);
            if (producers==null)
            {
                throw new Exception();
            }
            return producers;
        }
        public void AddEntity(PersonRequest producer)
        {
            _producerRepository.AddEntity(producer);
        }

        public void UpdateEntity(PersonRequest producer, int id)
        {
            var producerById = _producerRepository.GetEntity(id);
            if (producerById != null)
            {
                _producerRepository.UpdateEntity(producer, id);
            }
            else
            {
                throw new Exception();
            }
            

        }

        public void DeleteEntity(int id)
        {
            var producerById = _producerRepository.GetEntity(id);
            if (producerById != null)
            {
                _producerRepository.DeleteEntity(id);
            }
            else
            {
                throw new Exception();
            }

        }
    }
}
