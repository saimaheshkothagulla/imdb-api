﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
   public interface IGenreService
    {
        public IEnumerable<Genre> GetAll();
        public Genre GetEntity(int id);
        public IEnumerable<Genre> GetEntitiesByMovieId(int movieId);
        public void AddEntity(GenreRequest genre);
        public void UpdateEntity(GenreRequest genre, int id);
        public void DeleteEntity(int id);
    }
}
