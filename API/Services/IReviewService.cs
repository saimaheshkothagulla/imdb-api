﻿using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public interface IReviewService
    {
        public IEnumerable<Review> GetAll(int movieId);
        public Review GetEntity(int movieId, int reviewId);
        public void AddEntity(ReviewRequest review, int movieId);
        public void UpdateEntity(int movieId,ReviewRequest review, int reviewId);
        public void DeleteEntity(int movieId,int reviewId);

    }
}
