﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using Microsoft.AspNetCore.Mvc;
namespace DemoApp.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;


        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
           
            
        }

        public IEnumerable<Person> GetAll()
        {
           var actors=_actorRepository.GetAll();
            return actors;
        }
        public Person GetEntity(int id)
        {
            var actor= _actorRepository.GetEntity(id);
            if (actor!= null)
            {
                return actor;
            }
            throw new Exception();
            
        }
        public IEnumerable<Person> GetEntitiesByMovieId(int movieId)
        {
           var actors= _actorRepository.GetEntitiesByMovieId(movieId);
            if (actors.Count()==0)
            {
                throw new Exception();
            }
            return actors;
            
        }
        public void AddEntity(PersonRequest actor)
        {
           _actorRepository.AddEntity(actor);
        }

        public void UpdateEntity(PersonRequest actor, int id)
        {
            var actorById = _actorRepository.GetEntity(id);
            if (actorById != null)
            {
                _actorRepository.UpdateEntity(actor, id);
            }
            else
            {
                throw new Exception();
            }
            
            
        }

        public void DeleteEntity(int id)
        {
            var actorById = _actorRepository.GetEntity(id);
            if (actorById != null)
            {
                _actorRepository.DeleteEntity(id);
            }
            else
            {
                throw new Exception();
            }



        }
       
    }
}
