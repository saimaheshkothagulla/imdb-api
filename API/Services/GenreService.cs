﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
     

        public GenreService(IGenreRepository genreRepository){
            _genreRepository = genreRepository;
     
        }

       

        public IEnumerable<Genre> GetAll()
        {
            return _genreRepository.GetAll();
        }

        public Genre GetEntity(int id)
        {
            var genre = _genreRepository.GetEntity(id);
            if (genre != null)
            {
                return genre;
            }
            throw new Exception();
            
        }
        public IEnumerable<Genre> GetEntitiesByMovieId(int movieId)
        {
            var genres= _genreRepository.GetEntitiesByMovieId(movieId);
            if (genres.Count() == 0)
            {
                throw new Exception();
            }
            return genres;


        }


        public void AddEntity(GenreRequest genre)
        {
            _genreRepository.AddEntity(genre);
        }
        public void UpdateEntity(GenreRequest genre, int id)
        {
            var genreById = _genreRepository.GetEntity(id);
            if (genreById != null)
            {
                _genreRepository.UpdateEntity(genre, id);
            }
            else
            {
                throw new Exception();
            }
            
        }

        public void DeleteEntity(int id)
        {
            var genre = _genreRepository.GetEntity(id);
            if (genre != null)
            {
                _genreRepository.DeleteEntity(id);
            }
            else
            {
                throw new Exception();
            }

            
        }

    }
}
