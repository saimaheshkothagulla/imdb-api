﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Services;
using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Controllers
{
    [Route("movies")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_movieService.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var movie = _movieService.GetEntity(id);
                return Ok(movie);
            }
            catch (Exception)
            {
                return NotFound("Invalid Movie id");
            }


         }
        
        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movie)
        {
            _movieService.AddEntity(movie);
            return Ok("Successfully added..");
        }
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] MovieRequest movie, int id)
        {
            try
            {
                _movieService.UpdateEntity(movie, id);
                return Ok("Successfully upadated..");
            }
            catch (Exception)
            {
                return NotFound("not found given id");
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _movieService.DeleteEntity(id);
                return Ok("successfully deleted..");
            }
            catch (Exception)
            {
                return NotFound("invalid id");
            }
        }
        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");
            var task = await new FirebaseStorage("imdbapi-assignment.appspot.com")
                    .Child("images")
                    .Child(Guid.NewGuid().ToString() + ".jpg")
                    .PutAsync(file.OpenReadStream());
            return Ok(task);
        }


    }
}
