﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Repository;
using DemoApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Controllers
{
    [Route("producers")]
    [ApiController]
    public class ProducersController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducersController(IProducerService producerService)
        {
            _producerService = producerService;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_producerService.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var producer = _producerService.GetEntity( id);
                return Ok(producer);
            }
            catch (Exception)
            {
                return NotFound("invalid producer id");
            }

        }
        [HttpGet("movies/{movieId}")]
        public IActionResult GetByMovieId(int movieId)
        {
            try
            {
                var producers = _producerService.GetEntityByMovieId(movieId);
                return Ok(producers);
            }
            catch (Exception)
            {
                return NotFound("Invalid movie Id..");
            }

        }
        [HttpPost]
        public IActionResult Post([FromBody] PersonRequest producer)
        {
            _producerService.AddEntity( producer);
            return Ok("Successfully added..");
        }
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] PersonRequest producer, int id)
        {
            try
            {
                _producerService.UpdateEntity(producer, id);
                return Ok("Successfully upadated..");
            }
            catch (Exception)
            {
                return NotFound("Invalid producer id");
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _producerService.DeleteEntity( id);
                return Ok("successfully deleted..");
            }
            catch (Exception)
            {
                return NotFound("Invalid producer id");
            }
        }

    }
}
