﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DemoApp.Controllers
{
    [ApiController]
    [Route("genres")]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_genreService.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var actor = _genreService.GetEntity(id);
                return Ok(actor);
            }
            catch(Exception)
            {
                return NotFound("invalid genre id");
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genre)
        {
         
            _genreService.AddEntity(genre);
            return Ok("Successfully added..");
        }
        [HttpGet("movies/{movieId}")]
        public IActionResult GetByMovieId(int movieId)
        {
            try
            {
                var genres= _genreService.GetEntitiesByMovieId(movieId);
                return Ok(genres);
            }
            catch (Exception)
            {
                return NotFound("Invalid movie Id..");
            }

        }
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] GenreRequest genre, int id)
        {
            try
            {
                _genreService.UpdateEntity(genre, id);
                return Ok("Successfully upadated..");

            }
            catch (Exception)
            {
                return NotFound("invalid Genre Id");
            }
           
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _genreService.DeleteEntity(id);
                return Ok("successfully deleted..");
            }
            catch (Exception)
            {
                return NotFound("invalid genre id");
            }

        }

    }
}
