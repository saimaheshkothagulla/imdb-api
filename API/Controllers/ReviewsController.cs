﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Controllers
{
    [Route("movies/{movieId:int}/reviews")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }
        [HttpGet()]
        public IActionResult GetAll(int movieId)
        {
            try
            {
                var reviews = _reviewService.GetAll(movieId);
                return Ok(reviews);
            }
            catch
            {
                return NotFound("Invalid MovieId");
            }
        }
        [HttpGet("{reviewId}")]
        public IActionResult Get(int movieId,int reviewId)
        {
            try
            {
                var review = _reviewService.GetEntity(movieId,reviewId);
                return Ok(review);
            }
            catch (Exception)
            {
                return NotFound("invalid review id");
            }

        }
        [HttpPost]
        public IActionResult Post(int movieId,[FromBody] ReviewRequest review)
        {
            _reviewService.AddEntity(review,movieId);
            return Ok("Successfully added..");
        }
        [HttpPut("{reviewId}")]
        public IActionResult Put(int movieId,[FromBody] ReviewRequest review,int reviewId)
        {
           
                _reviewService.UpdateEntity(movieId,review, reviewId);
                return Ok("Successfully upadated..");
            
            
        }
        [HttpDelete("{reviewid}")]
        public IActionResult Delete(int movieId,int reviewId)
        {
            try
            {
                _reviewService.DeleteEntity(movieId,reviewId);
                return Ok("successfully deleted..");
            }
            catch (Exception)
            {
                return NotFound("review can't deleted");
            }
        }

    }
}
