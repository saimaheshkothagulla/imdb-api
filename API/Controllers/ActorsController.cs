﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Repository;
using DemoApp.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DemoApp.Controllers
{
    [ApiController]
    [Route("actors")]
    public class ActorsController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorsController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_actorService.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var actor = _actorService.GetEntity( id);
                return Ok(actor);
            }
            catch (Exception)
            {
                return NotFound("Invalid Actor Id..");
            }

        }

        [HttpGet("movies/{movieId}")]
        public IActionResult GetEntitiesByMovieId(int movieId)
        {
            try
            {
                var actors = _actorService.GetEntitiesByMovieId(movieId);
                return Ok(actors);
            }
            catch (Exception)
            {
                return NotFound("Invalid movie Id..");
            }

        }
        [HttpPost]
        public IActionResult Post([FromBody] PersonRequest actor)
        {
            _actorService.AddEntity(actor);
            return Ok("Successfully added..");
        }
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] PersonRequest actor, int id)
        {
            try
            {
                _actorService.UpdateEntity( actor, id);
                return Ok("Successfully upadated..");
            }
            catch (Exception)
            {
                return NotFound("invalid actor id");
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _actorService.DeleteEntity( id);
                return Ok("successfully deleted..");
            }
            catch (Exception)
            {
                return NotFound("Invalid actor id");
            }
        }

    }
}
