﻿using Dapper;
using DemoApp.Models;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly ConnectionString _connection;

        public MovieRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }

        public IEnumerable<Movie> GetAll()
        {

            string sql = @"SELECT * 
                        FROM Movies";
            
            using (var connection = new SqlConnection(_connection.DB))
            {
                var movies = connection.Query<Movie>(sql);
                return movies;
            }
        }
        public Movie GetEntity(int id)
        {
          
            string sql = @"SELECT * 
                        FROM Movies 
                        WHERE Id= @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var movie = connection.QueryFirst<Movie>(sql, new { id = id });
               
                
                return movie;
            }
        }
        public void AddEntity(MovieRequest movie)
        {

            string sql = @"usp_AddingMovie";
            
             

            using (var connection = new SqlConnection(_connection.DB))
            {
                 connection.Execute(sql,
                    new 
                    { name = movie.Name
                    , year = movie.YearOfRelease
                    , plot = movie.Plot
                    , producerId = movie.ProducerId
                    , profit = movie.Profit
                    , poster = movie.Poster
                    ,actorIds=string.Join(',',movie.Actors)
                    ,genreIds=string.Join(',',movie.Genres)
                    },
                    commandType:CommandType.StoredProcedure

                   ); 
                
                

            }
        }
        public void UpdateEntity(MovieRequest movie, int id)
        {
            string sql = @"usp_UpdateMovie";

            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, 
                new 
                { 
                    Name = movie.Name
                    , Year = movie.YearOfRelease
                    , Plot = movie.Plot
                    ,ProducerId = movie.ProducerId
                    , Profit = movie.Profit
                    , Poster = movie.Poster 
                    ,id=id
                    ,ActorIds=String.Join(',',movie.Actors)
                    ,GenreIds=String.Join(',',movie.Genres)
                },
                commandType: CommandType.StoredProcedure
                );

            }
        }
        public void DeleteEntity(int id)
        {
            string sql = @"usp_DeleteMovieById";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Execute(sql, new { id = id },commandType:CommandType.StoredProcedure);

            }
        }

      
    }
}
