﻿using Dapper;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private readonly ConnectionString _connection;

        public GenreRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        public IEnumerable<Genre> GetAll()
        {
            string sql = @"SELECT * 
                           FROM Genres";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var genre = connection.Query<Genre>(sql);
                return genre;
            }
        }

        public Genre GetEntity(int id)
        {

            string sql = @"SELECT * 
                            FROM Genres 
                            WHERE Id=@id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var genre = connection.QueryFirst<Genre>(sql, new { id = id });
                return genre;
            }
        }
        public IEnumerable<Genre> GetEntitiesByMovieId(int movieId)
        {
            string sql = @"SELECT G.Id
	                            ,G.Name
                            FROM MovieGenreRelationShip MGR
                            INNER JOIN Genres G ON MGR.genreID = G.Id
                            WHERE MGR.movieID = @movieID";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var genres = connection.Query<Genre>(sql, new { movieId =movieId });
                return genres;
            }

        }

        public void AddEntity(GenreRequest genre)
        {
            string sql = @"INSERT INTO 
                            Genres 
                            VALUES(@name)";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<GenreResponse>(sql, new { name = genre.Name });
                   

            }
        }
        public void UpdateEntity(GenreRequest genre, int id)
        {
            string sql = @"UPDATE Genres
                            SET Name=@name
                            WHERE Id=@id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<Genre>(sql, new { name = genre.Name, id = id });

            }
        }
        public void DeleteEntity(int id)
        {
            string sql = @"DELETE
                        FROM MovieGenreRelationship
                        WHERE GenreId = @id

                        DELETE
                        FROM Genres
                        WHERE Id = @id
                           ";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<Genre>(sql, new { id = id });

            }
        }

      
    }
}
