﻿using System.Collections.Generic;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;

namespace DemoApp.Repository
{
    public interface IActorRepository
    {
        public IEnumerable<Person> GetAll();
        public Person GetEntity(int id);
        public IEnumerable<Person> GetEntitiesByMovieId(int movieId);
        public void AddEntity(PersonRequest actor);
        public void UpdateEntity(PersonRequest actor, int id);
        public void DeleteEntity(int id);
    }
}
