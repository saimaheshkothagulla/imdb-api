﻿using DemoApp.Models;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> GetAll();
        public Movie GetEntity(int id);
        public void AddEntity(MovieRequest movie);
        public void UpdateEntity(MovieRequest movie, int id);
        public void DeleteEntity(int id);

    }
}
