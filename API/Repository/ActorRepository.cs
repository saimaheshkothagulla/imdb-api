﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using Microsoft.Extensions.Options;

namespace DemoApp.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly ConnectionString _connection;
        
        public ActorRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }

        /// <inheritdoc />
        public IEnumerable<Person> GetAll()
        {
            
            string sql = @"SELECT *
                           FROM Actors";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors = connection.Query<Person>(sql);
                return actors;
            }
        }

        public Person GetEntity(int id)
        {
            string sql = @"SELECT *
                        FROM Actors
                        WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var actor =connection.QueryFirst<Person>(sql,new { id=id});
                return actor;
            }
        }
        public void AddEntity(PersonRequest actor)
        {
            string sql = @"INSERT INTO Actors
                            VALUES (
	                            @name
	                            ,@gender
	                            ,@bio
	                            ,@dob
	                            )";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new {name=actor.Name,gender=actor.Gender,bio=actor.Bio,dob=actor.DOB });
                
            }
        }

        public void UpdateEntity(PersonRequest actor,int id)
        {
            string sql = @"UPDATE Actors
                            SET Name = @name
	                            ,Gender = @gender
	                            ,Bio = @bio
	                            ,DOB = @dob
                            WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new { name = actor.Name, gender = actor.Gender, bio = actor.Bio, dob = actor.DOB ,id=id});

            }

        }

        public void DeleteEntity(int id)
        {
            string sql = @"
                            DELETE
                            FROM MovieActorRelationship
                            WHERE actorID = @id

                            DELETE
                            FROM Actors
                            WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new { id = id });

            }
        }
        public IEnumerable<Person> GetEntitiesByMovieId(int movieId)
        {
            string sql = @"SELECT A.Id
	                            ,A.Name
	                            ,A.Gender
	                            ,A.Bio
	                            ,A.DOB
                            FROM MovieActorRelationship MAR
                            INNER JOIN Actors A ON MAR.actorID = A.Id
                            WHERE MAR.movieID = @movieId";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var actors=connection.Query<Person>(sql, new { movieId =movieId });
                return actors;


            }
        }
    }
}
