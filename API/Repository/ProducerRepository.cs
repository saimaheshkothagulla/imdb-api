﻿using Dapper;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public class ProducerRepository:IProducerRepository
    {
        private readonly ConnectionString _connection;

        public ProducerRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }

        /// <inheritdoc />
        public IEnumerable<Person> GetAll()
        {
            string sql = @"SELECT * 
                        FROM Producers";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var producers = connection.Query<Person>(sql);
                return producers;
            }
        }
        public Person GetEntityByMovieId(int movieId)
        {
            string sql = @"SELECT P.Id
	                            ,P.Name 
	                            ,P.Gender
	                            ,P.Bio
	                            ,P.DOB
                            FROM Movies M
                            INNER JOIN Producers P ON M.ProducerID = p.Id
                            WHERE M.Id = @movieId";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var producers = connection.QueryFirst<Person>(sql,new { movieId=movieId});
                return producers;
            }
        }
        public Person GetEntity(int id)
        {
            string sql = @"SELECT * 
                        FROM Producers 
                        WHERE Id= @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var producer = connection.QueryFirst<Person>(sql, new { id = id });
                return producer;
            }
        }
        public void AddEntity(PersonRequest producer)
        {
            string sql = @"INSERT INTO Producers
                            VALUES (
	                            @name
	                            ,@gender
	                            ,@bio
	                            ,@dob
	                            )";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new { name = producer.Name,gender=producer.Gender,bio = producer.Bio, dob = producer.DOB });

            }
        }

        public void UpdateEntity(PersonRequest producer, int id)
        {

            string sql = @"UPDATE Producers
                        SET Name = @name
	                        ,Gender = @gender
	                        ,DOB =@dob
                            ,Bio=@bio
                        WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new { name = producer.Name, gender = producer.Gender,bio = producer.Bio, dob = producer.DOB, id = id });

            }
        }

        public void DeleteEntity(int id)
        {
            string sql = @"DELETE 
                            FROM Movies 
                            WHERE ProducerId=@id

                           DELETE 
                            FROM Producers
                            WHERE Id=@id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<PersonResponse>(sql, new { id = id });

            }
        }
    }
}
