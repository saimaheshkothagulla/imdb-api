﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public interface IProducerRepository
    {
        public IEnumerable<Person> GetAll();
        public Person GetEntity(int id);
        public Person GetEntityByMovieId(int movieId);
        public void AddEntity(PersonRequest producer);
        public void UpdateEntity(PersonRequest producer, int id);
        public void DeleteEntity(int id);

    }
}
