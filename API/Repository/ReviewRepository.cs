﻿using Dapper;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Repository
{
    public class ReviewRepository : IReviewRepository
    {


        private readonly ConnectionString _connection;

        public ReviewRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        
        public IEnumerable<Review> GetAll(int movieId)
        {
            string sql = @"SELECT R.Id,R.Message
                            FROM Reviews R
                            INNER JOIN MovieReviewsRelationship MR ON R.Id = MR.ReviewId
                            WHERE MR.MovieId = @movieId";

            using (var connection = new SqlConnection(_connection.DB))
            {

                var reviews = connection.Query<Review>(sql, new { movieId = movieId });
                return reviews;
            }
        }

        public Review GetEntity(int movieId, int reviewId)
        {
            string sql = @"SELECT R.Id,R.Message
                            FROM Reviews R
                            INNER JOIN MovieReviewsRelationship MR ON R.Id = MR.ReviewId
                            WHERE MR.MovieId = @movieId
	                            AND R.Id = @reviewId";
            using (var connection = new SqlConnection(_connection.DB))
            {

                var review = connection.QueryFirst<Review>(sql, new { movieId = movieId ,reviewId=reviewId});
                return review;
            }
        }
        public void AddEntity(ReviewRequest review, int movieId)
        {
            string sql = @"INSERT INTO Reviews
                           VALUES(@review)

                        INSERT INTO MovieReviewsRelationship
                        VALUES(
                                @movieId
                                , SCOPE_IDENTITY()
                                )";

            using (var connection = new SqlConnection(_connection.DB))
            {

                connection.Execute(sql, new {  review = review.Message, movieId = movieId });

            }

        }

        public void UpdateEntity(int movieId,ReviewRequest review, int reviewId)
        {
            string sql = @"UPDATE Reviews
                            SET Message = @review
                            WHERE Id = @reviewId";
                       
            using (var connection = new SqlConnection(_connection.DB))
            {

                 connection.Execute(sql, new { reviewId = reviewId, review = review.Message,movieId=movieId });
               
            }

        }
        public void DeleteEntity(int movieId,int reviewId)
        {
            string sql = @"DELETE FROM MovieReviewsRelationship WHERE ReviewId=@reviewId
                          DELETE FROM Reviews WHERE Id=@reviewId
                           ";
            using (var connection = new SqlConnection(_connection.DB))
            {

                connection.Query<ReviewResponse>(sql, new { reviewId = reviewId });

            }
        }
    }
}
