﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Models.Response
{
    public class ReviewResponse
    {
        public int Id { get; set; }
        public String Message { get; set; }
    }
}
