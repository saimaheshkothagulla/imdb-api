﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public Person Producer { get; set; }
        public List<Person> Actors { get; set; }
        public List<Genre> Genres { get; set; }
        public int Profit { get; set; }
        public string Poster { get; set; }
    }
}
