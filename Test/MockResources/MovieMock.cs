﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class MovieMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IMovieRepository> MovieRepositoryMock = new Mock<IMovieRepository>();
        private static readonly IEnumerable<Movie> ListOfMovies = new List<Movie>
        {
            new Movie
            {
                Id = 1,
                Name = "Movie1",
                YearOfRelease=2019,
                Profit=20000,
                Plot="Plot",
                ProducerId=1,
                Poster="movie1.jpg"
            }

        };
        public static void MockMovieRepositoryImplementation()
        {
            MovieRepositoryMock.Setup(x => x.GetAll()).Returns(ListOfMovies);
            MovieRepositoryMock.Setup(x => x.GetEntity(It.IsAny<int>()))
               .Returns((int id) => ListOfMovies.SingleOrDefault(
                   m => m.Id == id)

               );

            MovieRepositoryMock.Setup(x => x.AddEntity(It.IsAny<MovieRequest>()));
            MovieRepositoryMock.Setup(x => x.UpdateEntity(It.IsAny<MovieRequest>(), It.IsAny<int>()));

            MovieRepositoryMock.Setup(x => x.DeleteEntity(It.IsAny<int>()));
        }
       
                
    }
}