﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class ActorMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IActorRepository> ActorRepositoryMock = new Mock<IActorRepository>();
        private static readonly IEnumerable<Person> ListOfActors = new List<Person>
        {
            new Person
            {
                Id = 1,
                Name = "Actor1",
                Gender="Male",
                Bio = "Bio1",
                DOB =new DateTime(1999,10,10)
            }

        };
        public static void MockActorRepositotyImplementation()
        {
            ActorRepositoryMock.Setup(x => x.GetAll()).Returns(ListOfActors);
            ActorRepositoryMock.Setup(x => x.GetEntitiesByMovieId(It.IsAny<int>()))
               .Returns((int id) => {
                   var actorsId = MovieActorMapping(id);
                   if (actorsId == null)
                   {
                       return null;
                   }
                   else
                   {
                       var actors = ListOfActors.Where(a => actorsId.Contains(a.Id));
                       return actors;

                   }

               });
            ActorRepositoryMock.Setup(x => x.GetEntity(It.IsAny<int>()))
            .Returns((int id) => ListOfActors.SingleOrDefault(x => x.Id == id));
            ActorRepositoryMock.Setup(x => x.AddEntity(It.IsAny<PersonRequest>()));
            ActorRepositoryMock.Setup(x => x.UpdateEntity(It.IsAny<PersonRequest>(), It.IsAny<int>()));
            ActorRepositoryMock.Setup(x => x.DeleteEntity(It.IsAny<int>()));
        }
        

        
        public static List<int> MovieActorMapping(int id)
        {
            var MovieActorMappingDictionary = new Dictionary<int, List<int>>();
            var ListOfActorsId = new List<int>() { 1 };
            MovieActorMappingDictionary.Add(1, ListOfActorsId);
            var result = MovieActorMappingDictionary.ContainsKey(id) ? MovieActorMappingDictionary[id] : null;
            return result;
        }
    }
}