﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoApp.Models;
using DemoApp.Models.DB;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class ReviewMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IReviewRepository> ReviewRepositoryMock = new Mock<IReviewRepository>();
        private static readonly IEnumerable<Review> ListOfReviews = new List<Review>
        {
            new Review
            {
                Id = 1,
               Message = "Review1",
              
            }

        };
        public static void MockReviewRepositoryImplementation()
        {
            ReviewRepositoryMock.Setup(x => x.GetAll(It.IsAny<int>()))
                    .Returns(ListOfReviews);
            ReviewRepositoryMock.Setup(x => x.GetEntity(It.IsAny<int>(), It.IsAny<int>()))
                 .Returns((int movieId, int reviewId) => {
                     var reviewsId = MovieReviewMapping(movieId, reviewId);
                     if (reviewsId == null)
                     {
                         return null;
                     }
                     else
                     {
                         var review = ListOfReviews.Where(a => reviewsId.Contains(a.Id)).FirstOrDefault();
                         return review;

                     }

                 });
            ReviewRepositoryMock.Setup(x => x.AddEntity(It.IsAny<ReviewRequest>(), It.IsAny<int>()));


            ReviewRepositoryMock.Setup(x => x.UpdateEntity(It.IsAny<int>(), It.IsAny<ReviewRequest>(), It.IsAny<int>()));

            ReviewRepositoryMock.Setup(x => x.DeleteEntity(It.IsAny<int>(), It.IsAny<int>()));
        }
       
        
        public static List<int> MovieReviewMapping(int movieId,int reviewId)
        {
            var MovieReviewMappingDictionary = new Dictionary<int, List<int>>();
            var ListOfReviewsId = new List<int>() { 1 };
            MovieReviewMappingDictionary.Add(1, ListOfReviewsId);
            var result = MovieReviewMappingDictionary.ContainsKey(movieId) ? MovieReviewMappingDictionary[movieId] : null;
            return result;
        }

    }
}