﻿using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.MockResources
{
    class ProducerMock
    {
        /// </summary>
        public static readonly Mock<IProducerRepository> ProducerRepositoryMock = new Mock<IProducerRepository>();
        private static readonly IEnumerable<Person> ListOfProducers = new List<Person>
        {
            new Person
            {
                Id = 1,
                Name = "Producer1",
                Gender="Male",
                Bio = "Bio1",
                DOB =new DateTime(1999,10,10)
            }

        };
        public static void MockProducerRepositoryImplementation()
        {
            ProducerRepositoryMock.Setup(x => x.GetAll()).Returns(ListOfProducers);
            ProducerRepositoryMock.Setup(x => x.GetEntityByMovieId(It.IsAny<int>()))
                .Returns((int id) => {
                    var producerId = MovieProducerMapping(id);
                    if (producerId == 0)
                    {
                        return null;
                    }
                    else
                    {
                        var producer = ListOfProducers.Where(a => a.Id == producerId).FirstOrDefault();
                        return producer;

                    }

                });
            ProducerRepositoryMock.Setup(x => x.GetEntity(It.IsAny<int>()))
               .Returns((int id) => ListOfProducers.SingleOrDefault(x => x.Id == id));
            ProducerRepositoryMock.Setup(x => x.AddEntity(It.IsAny<PersonRequest>()));
            ProducerRepositoryMock.Setup(x => x.UpdateEntity(It.IsAny<PersonRequest>(), It.IsAny<int>()));
            ProducerRepositoryMock.Setup(x => x.DeleteEntity(It.IsAny<int>()));
        }
       
        public static int MovieProducerMapping(int id)
        {
            var MovieProducerMappingDictionary = new Dictionary<int, int>();
            var producerId = 1;
            MovieProducerMappingDictionary.Add(1, producerId);
            var result = MovieProducerMappingDictionary.ContainsKey(id) ? MovieProducerMappingDictionary[id] : 0;
            return result;
        }
    }
}