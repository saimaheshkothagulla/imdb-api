﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoApp.Models;
using DemoApp.Models.Request;
using DemoApp.Models.Response;
using DemoApp.Repository;
using Moq;

namespace DemoApp.Test.MockResources
{
    public class GenreMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IGenreRepository> GenreRepositoryMock = new Mock<IGenreRepository>();
        private static readonly IEnumerable<Genre> ListOfGenres = new List<Genre>
        {
            new Genre
            {
                Id = 1,
                Name = "Genre1"

            }

        };
        public static void MockGenreRepositoryImplementation()
        {
            GenreRepositoryMock.Setup(x => x.GetAll()).Returns(ListOfGenres);
            GenreRepositoryMock.Setup(x => x.GetEntitiesByMovieId(It.IsAny<int>()))
                .Returns((int id) => {
                    var genresId = MovieGenreMapping(id);
                    if (genresId == null)
                    {
                        return null;
                    }
                    else
                    {
                        var actors = ListOfGenres.Where(a => genresId.Contains(a.Id));
                        return actors;

                    }

                });
            GenreRepositoryMock.Setup(x => x.GetEntity(It.IsAny<int>()))
               .Returns((int id) => ListOfGenres.SingleOrDefault(x => x.Id == id));
            GenreRepositoryMock.Setup(x => x.AddEntity(It.IsAny<GenreRequest>()));
            GenreRepositoryMock.Setup(x => x.UpdateEntity(It.IsAny<GenreRequest>(), It.IsAny<int>()));
            GenreRepositoryMock.Setup(x => x.DeleteEntity(It.IsAny<int>()));

        }
    
        public static List<int> MovieGenreMapping(int id)
        {
            var MovieGenresMappingDictionary = new Dictionary<int, List<int>>();
            var ListOfGenresId = new List<int>() { 1 };
            MovieGenresMappingDictionary.Add(1, ListOfGenresId);
            var result = MovieGenresMappingDictionary.ContainsKey(id) ? MovieGenresMappingDictionary[id] : null;
            return result;
        }
    }
}