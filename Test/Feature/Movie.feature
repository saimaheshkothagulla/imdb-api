﻿Feature: Movie Resource

Scenario: Get All Movies
	Given I am a client
	When I make GET Request '/movies' 
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Movie1","yearOfRelease":2019,"plot":"Plot","producer":{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"},"actors":[{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}],"genres":[{"id":1,"name":"Genre1"}],"profit":20000,"poster":"movie1.jpg"}]'
	
Scenario: Get Movie By MovieId
	Given I am a client
	When I make GET Request '/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Movie1","yearOfRelease":2019,"plot":"Plot","producer":{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"},"actors":[{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}],"genres":[{"id":1,"name":"Genre1"}],"profit":20000,"poster":"movie1.jpg"}'
	
Scenario: Get Movie By Wrong MovieId
	Given I am a client
	When I make GET Request '/movies/10'
	Then response code must be '404'

Scenario: Create Movie
	Given I am a client
	When I am making a POST request to '/movies' with the following Data '{"name":"M2","dateOfRelease":2020,"plot":"plot2","producerId":1,"actors":[1,2],"genres":[1],"profit":200,"poster":"movie2.jpg"}]'
	Then response code must be '200'

Scenario: Update Movie By MovieId
	Given I am a client
	When I make PUT Request '/movies/1' with the following Data  '{"name":"Movie1","yearOfRelease":2019,"plot":"Plot","producer":1,"actors":[1],"genres":[1],"profit":20000,"poster":"movie1.jpg"}'
	Then response code must be '200'

Scenario: Update Movie By Wrong MovieId
	Given I am a client
	When I make PUT Request '/movies/10' with the following Data  '{"name":"Movie1","yearOfRelease":2019,"plot":"Plot","producer":1,"actors":[1],"genres":[1],"profit":20000,"poster":"movie1.jpg"}'
	Then response code must be '404'

Scenario: Delete Movie By MovieId
	Given I am a client
	When I make Delete Request '/movies/1'
	Then response code must be '200'

Scenario: Delete Movie By Wrong MovieId
	Given I am a client
	When I make Delete Request '/movies/10'
	Then response code must be '404'
