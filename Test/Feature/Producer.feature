﻿Feature: Producer Resource

Scenario: Get Producers All
	Given I am a client
	When I make GET Request '/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}]'


Scenario: Get Producer By Id
	Given I am a client
	When I make GET Request '/producers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'

Scenario: Get Producer By Wrong Id
	Given I am a client
	When I make GET Request '/producers/10'
	Then response code must be '404'

Scenario: Get Producers By Movie Id
	Given I am a client
	When I make GET Request '/producers/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'

Scenario: Get Producers By Wrong Movie Id
	Given I am a client
	When I make GET Request '/producers/movies/10'
	Then response code must be '404'

Scenario: Create An Producer
	Given I am a client
	When I am making a POST request to '/producers' with the following Data '{"id":2,"name":"Producer2","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '200'

Scenario: Update Producer By Producer Id
	Given I am a client
	When I make PUT Request '/producers/1' with the following Data  '{"id":1,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '200'

Scenario: Update Producer By Wrong Producer Id
	Given I am a client
	When I make PUT Request '/producers/10' with the following Data  '{"id":10,"name":"Producer1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '404'


Scenario:Delete Producer By Id
	Given I am a client
	When I make Delete Request 'producers/1'
	Then response code must be '200'

Scenario:Delete Producer By Wrong Id
	Given I am a client
	When I make Delete Request 'producers/10'
	Then response code must be '404'

