﻿Feature: Actor Resource

Scenario: Get Actors All
	Given I am a client
	When I make GET Request '/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}]'

Scenario: Get Actor By Id
	Given I am a client
	When I make GET Request '/actors/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'

Scenario: Get Actor By Wrong Id
	Given I am a client
	When I make GET Request '/actors/8'
	Then response code must be '404'
	And response data must look like 'Invalid Actor Id..'

Scenario: Get Actors By Movie Id
	Given I am a client
	When I make GET Request '/actors/movies/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}]'

Scenario: Get Actors By Wrong Movie Id
	Given I am a client
	When I make GET Request '/actors/movies/10'
	Then response code must be '404'
	And response data must look like 'Invalid movie Id..'

Scenario: Create An Actor
	Given I am a client
	When I am making a POST request to '/actors' with the following Data '{"id":3,"name":"Actor3","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '200'

Scenario: Update Actor By Actor Id
	Given I am a client
	When I make PUT Request '/actors/1' with the following Data  '{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '200'
	And response data must look like 'Successfully upadated..'


Scenario: Update Actor By Wrong Id
	Given I am a client
	When I make PUT Request '/actors/10' with the following Data  '{"id":1,"name":"Actor1","gender":"Male","bio":"Bio1","dob":"1999-10-10T00:00:00"}'
	Then response code must be '404'

Scenario:Delete Actor By Id
	Given I am a client
	When I make Delete Request 'actors/1'
	Then response code must be '200'

Scenario: Delete Actor By Wrong Id
	Given I am a client
	When I make Delete Request 'actors/8'
	Then response code must be '404'
	And response data must look like 'Invalid actor id'


