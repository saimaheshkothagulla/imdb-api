﻿Feature: Genre Resource

Scenario: Get Genres All
	Given I am a client
	When I make GET Request '/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Genre1"}]'


Scenario: Get Genre By Id
	Given I am a client
	When I make GET Request '/genres/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Genre1"}'


Scenario: Get Genre By Wrong Id
	Given I am a client
	When I make GET Request '/genres/10'
	Then response code must be '404'
	And response data must look like 'invalid genre id'

Scenario: Get Genres By Movie Id
	Given I am a client
	When I make GET Request '/genres/movies/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Genre1"}]'


Scenario: Get Genres By Wrong Movie Id
	Given I am a client
	When I make GET Request '/genres/movies/10'
	Then response code must be '404'
	And response data must look like 'Invalid movie Id..'

Scenario: Create An Genre
	Given I am a client
	When I am making a POST request to '/genres' with the following Data '{"id":3,name:"Genre3"}'
	Then response code must be '200'

Scenario: Update Genre By Genre Id
	Given I am a client
	When I make PUT Request '/genres/1' with the following Data  '{"id":1,"name":"Genre1"}'
	Then response code must be '200'

Scenario: Update Genre By Wrong Genre Id
	Given I am a client
	When I make PUT Request '/genres/10' with the following Data  '{"id":10,"name":"Genre1"}'
	Then response code must be '404'

Scenario:Delete Genre By Id
	Given I am a client
	When I make Delete Request 'genres/1'
	Then response code must be '200'


Scenario:Delete Genre By Wrong Id
	Given I am a client
	When I make Delete Request 'genres/10'
	Then response code must be '404'

