﻿Feature: Review Resource
	

Scenario: Get All Reviews By MovieId
	Given I am a client
	When I make GET Request '/movies/1/reviews'
	Then response code must be '200'
	And response data must look like '[{"id":1,"message":"Review1"}]'

Scenario: Get Review By MovieId and ReviewId
	Given I am a client
	When I make GET Request '/movies/1/reviews/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"message":"Review1"}'

Scenario: Create Review to Movie By MovieId
	Given I am a client
	When I am making a POST request to '/movies/1/reviews' with the following Data '{"id":23,"message":"Review12"}''
	Then response code must be '200'

Scenario: Update Review By MovieId and Review Id
	Given I am a client
	When I make PUT Request '/movies/1/reviews/1' with the following Data  '{"message":"Review12"}'
	Then response code must be '200'


Scenario: Delete Review By MovieId  and ReviewId
	Given I am a client
	When I make Delete Request '/movies/1/reviews/1'
	Then response code must be '200'


