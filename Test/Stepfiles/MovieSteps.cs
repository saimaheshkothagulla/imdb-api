﻿using DemoApp.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using Test.MockResources;

namespace DemoApp.Test.StepFiles
{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Mock Repo
                    services.AddScoped(_ => MovieMock.MovieRepositoryMock.Object);
                    services.AddScoped(service=> ActorMock.ActorRepositoryMock.Object);
                    services.AddScoped(service => ProducerMock.ProducerRepositoryMock.Object);
                    services.AddScoped(service => GenreMock.GenreRepositoryMock.Object);
                    services.AddScoped(service => MovieMock.MovieRepositoryMock.Object);
                   

                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockMovieRepositoryImplementation();
            ActorMock.MockActorRepositotyImplementation();
            ProducerMock.MockProducerRepositoryImplementation();
            GenreMock.MockGenreRepositoryImplementation();
           
        
        }
    }
}